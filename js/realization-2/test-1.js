function quickSort(array) {
	if (array.length < 2) return array;

	// Пилотным элементом может быть любой
	// Первый, средний, рандомный
	const pivotItem = array[0];
	const lessArray = [];
	const moreArray = [];

	for (let i = 1; i < array.length; i++) {
		if (array[i] <= pivotItem) {
			lessArray.push(array[i]);
		} else {
			moreArray.push(array[i]);
		}
	}

	return [...quickSort(lessArray), pivotItem, ...quickSort(moreArray)];
}

export { quickSort };
