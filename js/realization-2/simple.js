function quickSort(array) {
	if (array.length < 2) return array;

	const pivot = array[Math.floor(array.length / 2)];
	const less = array.filter(item => item < pivot);
	const greater = array.filter(item => item > pivot);
	const equal = array.filter(item => item === pivot);

	return [...quickSort(less), ...equal, ...quickSort(greater)];
}

export { quickSort };
