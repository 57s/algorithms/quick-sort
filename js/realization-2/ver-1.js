function quickSort(array) {
	if (array.length < 2) return array;

	const pivotIndex = Math.floor(array.length / 2);
	const pivotItem = array[pivotIndex];
	const lessArray = [];
	const moreArray = [];

	for (let i = 0; i < array.length; i++) {
		// Что бы не выходить в бесконечную рекурсию
		if (i === pivotIndex) continue;

		if (array[i] <= pivotItem) {
			lessArray.push(array[i]);
		} else {
			moreArray.push(array[i]);
		}
	}

	return [...quickSort(lessArray), pivotItem, ...quickSort(moreArray)];
}

export { quickSort };
