function sortString(array, letter) {
	const arrayFiltered = array.filter(
		item => !new RegExp(`[${letter}]`, 'i').test(item)
	);

	function sort(arr) {
		if (arr.length <= 1) return arr;

		const pivot = Math.floor(arr.length / 2);
		const low = [];
		const more = [];
		const equal = [];

		for (let i = 0; i < arr.length; i++) {
			if (arr[i].localeCompare(arr[pivot], 'ru') < 0) {
				low.push(arr[i]);
			} else if (arr[i].localeCompare(arr[pivot], 'ru') > 0) {
				more.push(arr[i]);
			} else {
				equal.push(arr[i]);
			}
		}

		return [...sort(low), ...equal, ...sort(more)];
	}

	return sort(arrayFiltered);
}

export { sortString };
