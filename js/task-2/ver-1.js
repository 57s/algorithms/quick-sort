function findMinAndMax(array, n = 1) {
	const swap = (arr, a, b) => ([arr[b], arr[a]] = [arr[a], arr[b]]);

	function sortHelper(arr, low, more) {
		const pivot = Math.floor((low + more) / 2);

		while (low <= more) {
			while (arr[low] < arr[pivot]) low++;
			while (arr[more] > arr[pivot]) more--;

			if (low <= more) {
				swap(arr, low, more);

				low++;
				more--;
			}
		}

		return low;
	}

	function sort(arr, low = 0, more = arr.length - 1) {
		if (arr.length <= 1) return arr;

		const index = sortHelper(arr, low, more);

		if (low < index - 1) sort(arr, low, index - 1);
		if (more > index - 1) sort(arr, index, more);

		return arr;
	}

	const result = sort(array);

	return [result.at(n - 1), result.at(-n)];
}

export { findMinAndMax };
