function sortEven(array) {
	const arrayEven = array.filter(item => item % 2);

	function swap(arr, a, b) {
		[arr[b], arr[a]] = [arr[a], arr[b]];
		return arr;
	}

	function sortHelper(arr, left, right) {
		const pivot = Math.floor((left + right) / 2);

		while (right >= left) {
			while (arr[left] < arr[pivot]) left++;
			while (arr[right] > arr[pivot]) right--;

			if (left <= right) {
				swap(arr, left, right);
				left++;
				right--;
			}
		}

		return left;
	}

	function sort(arr, left = 0, right = arr.length - 1) {
		if (arr.length < 2) return arr;

		const index = sortHelper(arr, left, right);

		if (left < index - 1) sort(arr, left, index - 1);
		if (left > index - 1) sort(arr, index, right);

		return arr;
	}

	return sort(arrayEven);
}

export { sortEven };
