// Функция рандомизации
const getRandomNumber = function (min = 0, max = 1_000) {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

// Генерация рандомно массива
let arrayRandom = new Array(100);
arrayRandom.fill(0);
arrayRandom = arrayRandom.map(() => getRandomNumber());

let arrayBig = [
	986, 287, 634, 997, 184, 140, 357, 859, 708, 8, 940, 929, 482, 368, 906, 963,
	370, 353, 610, 983, 264, 605, 57, 901, 338, 786, 559, 894, 754, 874, 422, 799,
	113, 926, 421, 25, 450, 484, 411, 174, 490, 816, 478, 973, 606, 660, 313, 437,
	506, 347, 178, 849, 979, 921, 872, 493, 609, 231, 919, 548, 724, 734, 104,
	100, 831, 87, 335, 813, 22, 122, 82, 388, 866, 220, 987, 850, 162, 161, 956,
	240, 86, 470, 300, 843, 129, 486, 998, 965, 829, 559, 848, 899, 82, 133, 936,
	886, 591, 898, 145, 655,
];

let arrayTen = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
let arrayTenReverse = [...arrayTen].reverse();
let arraySmall = [10, 20, 5];
let array = [70, 1, 100, 50, 25, 70, 80, 90, 5, 3];
let arrayDuplicates = [10, 1, 1, 50, 10, 1, 100, 1000, 5, 100];

let arrayRuString = [
	'яблоко',
	'мандарин',
	'арбуз',
	'клубника',
	'банан',
	'Клубника',
	'Яблоко',
	'Ёгурт',
	'Банан',
	'Ананас',
	'Дыня',
	'Мандарин',
];
let arrayRuLettersReverse = ['Ё', 'Е', 'Д', 'Г', 'В', 'Б', 'A'];
let arrayStringData = ['20.03.2002', '19.03.2002', '01.05.2019', '05.09.2016'];

let arrayObjects = [
	{ name: 'Иван', age: 20, isAdmin: true },
	{ name: 'Кирил', age: 30, isAdmin: true },
	{ name: 'Юлия', age: 19, isAdmin: false },
	{ name: 'Сергей', age: 24, isAdmin: true },
];

export {
	arrayRandom,
	arrayBig,
	arrayTen,
	arrayTenReverse,
	arraySmall,
	array,
	arrayDuplicates,
	arrayRuString,
	arrayRuLettersReverse,
	arrayStringData,
	arrayObjects,
};
