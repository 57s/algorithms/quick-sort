function partition(array, low, top) {
	const pivotPosition = Math.floor(Math.random() * array.length);
	const pivot = array[pivotPosition];

	while (top >= low) {
		while (array[low] < pivot) low++;
		while (array[top] > pivot) top--;

		if (top >= low) {
			[array[top], array[low]] = [array[low], array[top]];
			low++;
			top--;
		}
	}

	return low;
}

function quickSort(array, low = 0, top = array.length - 1) {
	if (low >= top) return array;

	const index = partition(array, low, top);
	quickSort(array, low, index - 1);
	quickSort(array, index, top);

	return array;
}

export { quickSort };
