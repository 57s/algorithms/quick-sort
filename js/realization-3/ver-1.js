// Сложная реализация.
// Экономия памяти

function quickSort(array) {
	return quickSortHelper(array, 0, array.length - 1);
}

function quickSortHelper(array, leftIndex, rightIndex) {
	// Базовый случай
	if (array.length < 2) return array;

	// partition вернет индекс опорного элемента
	// элементы левее этого индекса будут меньше опорного элемента
	// а элементы правее больше.
	const index = partition(array, leftIndex, rightIndex);

	// Рекурсивные случаи
	// Нужно ли сортировать левую часть
	if (leftIndex < index - 1) quickSortHelper(array, leftIndex, index - 1);
	// Нужно ли сортировать правую часть
	if (rightIndex > index - 1) quickSortHelper(array, index, rightIndex);

	return array;
}

// partition ищет две позиции которые нужно поменять местами
// partition ищет первый левый элемент который больше pivot
// также ищет первый левый элемент который меньше pivot
// После запускает swap что бы поменять их местами
function partition(array, leftIndex, rightIndex) {
	const pivot = array[Math.floor((leftIndex + rightIndex) / 2)];

	while (leftIndex <= rightIndex) {
		while (array[leftIndex] < pivot) leftIndex++;
		while (array[rightIndex] > pivot) rightIndex--;

		if (leftIndex <= rightIndex) {
			// Переставить элементы в массиве
			swap(array, leftIndex, rightIndex);
			// Сдвинуть указатели после перестановки
			leftIndex++;
			rightIndex--;
		}
	}

	return leftIndex;
}

// swap меняет элементы массива местами
function swap(array, a, b) {
	const temp = array[a];
	array[a] = array[b];
	array[b] = temp;

	return array;
}

export { quickSort };
