function quickSort(array, leftIndex = 0, rightIndex = array.length - 1) {
	if (array.length < 2) return array;

	function partition(array, leftIndex, rightIndex) {
		const pivot = array[Math.floor((leftIndex + rightIndex) / 2)];

		while (leftIndex <= rightIndex) {
			while (array[leftIndex] < pivot) leftIndex++;
			while (array[rightIndex] > pivot) rightIndex--;

			if (leftIndex <= rightIndex) {
				const temp = array[leftIndex];
				array[leftIndex] = array[rightIndex];
				array[rightIndex] = temp;

				leftIndex++;
				rightIndex--;
			}
		}

		return leftIndex;
	}

	const index = partition(array, leftIndex, rightIndex);

	if (leftIndex < index - 1) quickSort(array, leftIndex, index - 1);
	if (rightIndex > index - 1) quickSort(array, index, rightIndex);

	return array;
}

export { quickSort };
