function quickSort(array) {
	if (array.length <= 1) return array;

	const middle = array[Math.floor(Math.random() * array.length)];
	const left = [];
	const right = [];
	const equal = [];

	for (let i = 0; i < array.length; i++) {
		if (array[i] < middle) {
			left.push(array[i]);
		} else if (array[i] > middle) {
			right.push(array[i]);
		} else {
			equal.push(array[i]);
		}
	}

	return [...quickSort(left), ...equal, ...quickSort(right)];
}

export { quickSort };
