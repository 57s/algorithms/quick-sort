# Quick sort

Быструю сортировку изобрел английский информатик **Тони Хоар**
во время своей работы в МГУ в _1960_ году.

Алгоритм быстрой сортировки один из самых быстрых.

В _JavaScript_ метод `.sort()`
использует алгоритм быстрой сортировки **quick sort**
для массивов с более чем 10 элементами
и алгоритм сортировки вставками (insertion sort) для массивов с менее чем 10 элементами.

## Complexity

`O(log(n) * n)`

## Link

- [1](https://www.youtube.com/watch?v=SCcioLRO66Q&list=PLjv_imdSY644Dt5cSwFoDpcwN_us_O2fd&index=6)
- [wiki](https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0)
- [Оптимизированная по памяти](https://www.guru99.com/quicksort-in-javascript.html)

## Visualization

![Разделение](res/quick-sort-1.jpg)
![Перестановка](res/quick-sort-2.jpg)
